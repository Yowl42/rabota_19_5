#include <iostream>
#include <cstdlib>

class Animal
{
public: 
	virtual void voice()
	{
		std::cout << "���� ����� �� ���������";
	}
};

class Cat : public Animal
{
	void voice() override
	{
		std::cout << "���" << std::endl;
	}
};

class Dog : public Animal
{
	void voice() override
	{
		std::cout << "���" << std::endl;
	}
};

class Cow : public Animal
{
	void voice() override
	{
		std::cout << "��y" << std::endl;
	}
};

int main() 
{
	//srand(time(0));
	setlocale(LC_ALL, "RU");
	int Amount = 0;

	std::cout << "���������� ������:";
	std::cin >> Amount;
	std::cout << "���" << std::endl;

	Animal** AnimalArray = new Animal*[Amount];
	for (int i = 0;i < Amount;i++)
	{
		int j = rand();// % 3;
		switch (j % 3)
		{
			case 0: 
				AnimalArray[i] = new Cat();
				break;
			case 1: 
				AnimalArray[i] = new Dog();
				break;
			case 2: 
				AnimalArray[i] = new Cow();
		}
		AnimalArray[i]->voice();
	}
	delete[] AnimalArray;

}